#!/bin/sh
cp ${SRCROOT}/libfmodex.dylib ${CONFIGURATION_BUILD_DIR}/${TARGET_NAME}.app/Contents/MacOs/
cp ${SRCROOT}/libfmodevent.dylib ${CONFIGURATION_BUILD_DIR}/${TARGET_NAME}.app/Contents/MacOs/
cd ${CONFIGURATION_BUILD_DIR}/${TARGET_NAME}.app/Contents/MacOs/
install_name_tool -change ./libfmodex.dylib @executable_path/libfmodex.dylib libfmodevent.dylib
