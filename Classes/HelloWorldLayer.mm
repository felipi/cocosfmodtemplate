//
//  HelloWorldLayer.m
//  Teste1
//
//  Created by Felipi Macedo on 4/10/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

//AUDIO-FMOD
#include "fmod_event.hpp"
#include "fmod.hpp"

#include "fmod_errors.h"

//FMOD
const float UPDATE_INTERVAL = 100.0f;

void ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}

FMOD::EventSystem    *eventsystem;
FMOD::MusicSystem	 *musicsystem;
FMOD::EventGroup     *eventgroup;
FMOD::EventCategory  *mastercategory;
FMOD::Event          *car;
FMOD::EventParameter *rpm;
FMOD::EventParameter *load;
FMOD::MusicPrompt	 *r_prompt;
FMOD::MusicPrompt	 *h_prompt;
FMOD::Sound			 *sound;
FMOD::System		 *fmodsystem;
FMOD_RESULT           result;

int                   key;
float* fmodp_speed, fmodp_time;

void fmodInit(){
	NSString* bundlePath = [[NSBundle mainBundle] bundlePath];
	NSString* mediaPath = [NSString stringWithFormat:@"%@/Contents/Resources/Media/", bundlePath];
    ERRCHECK(result = FMOD::EventSystem_Create(&eventsystem));
	
    ERRCHECK(result = eventsystem->init(64, FMOD_INIT_NORMAL, 0, FMOD_EVENT_INIT_NORMAL));
	ERRCHECK(result = eventsystem->setMediaPath([mediaPath cStringUsingEncoding:NSASCIIStringEncoding]));
    ERRCHECK(result = eventsystem->load("pepe.fev", 0, 0));
	//ERRCHECK(result = fmodsystem->createSound("pepe.fsb", FMOD_CREATESTREAM, 0, &sound));
	//ERRCHECK(result = eventsystem->preloadFSB("pepe.fsb", 0, sound));
	ERRCHECK(result = eventsystem->getMusicSystem(&musicsystem));
	ERRCHECK(result = musicsystem->loadSoundData(FMOD_EVENT_RESOURCE_SAMPLES, FMOD_EVENT_NONBLOCKING));	
	
	FMOD_MUSIC_ITERATOR mit;
	
	ERRCHECK(result = musicsystem->getParameters(&mit, "speed"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, 0.0f));
	ERRCHECK(result = musicsystem->getParameters(&mit, "time"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, 120.0f));
	
	ERRCHECK(result = musicsystem->setVolume(0.65f));
	
	ERRCHECK(result = musicsystem->getCues(&mit, "rythm"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &r_prompt));
	ERRCHECK(result = musicsystem->getCues(&mit, "harmony"));
	ERRCHECK(result = musicsystem->prepareCue(mit.value->id, &h_prompt));
	
	ERRCHECK(result = r_prompt->begin());
	ERRCHECK(result = h_prompt->begin());
	
}

void AudioUpdate(){
	FMOD_MUSIC_ITERATOR mit;
	
	ERRCHECK(result = musicsystem->getParameters(&mit, "speed"));
	ERRCHECK(result = musicsystem->setParameterValue(mit.value->id, 1));
	
	ERRCHECK(result = eventsystem->update());
}

void AudioDestroy(){
	
	//ERRCHECK(result = eventgroup->freeEventData());
	ERRCHECK(result = r_prompt->end());
	ERRCHECK(result = r_prompt->release());
	ERRCHECK(result = h_prompt->end());
	ERRCHECK(result = h_prompt->release());
	ERRCHECK(result = musicsystem->freeSoundData());
    ERRCHECK(result = eventsystem->release());
	
}
//==


// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		fmodInit();
		// create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:64];
		
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height/2 );
		
		// add the label as a child to this Layer
		[self addChild: label];
		[self schedule: @selector(tick:)];
	}
	return self;
}

- (void) tick: (ccTime) dt {
	AudioUpdate();
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	AudioDestroy();
	[super dealloc];
}
@end
