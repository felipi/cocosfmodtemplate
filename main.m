//
//  main.m
//  Template
//
//  Created by Felipi Macedo on 4/17/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"TemplateAppDelegate");
	[pool release];
	return retVal;
}
